import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
      fontFamily: {
        sans: ['Raleway', 'sans-serif'],
      },
      colors: {
        "purpleouais": "#8121D0",
        "purplee" :    "#291C3A",
        "purplehello" : "#331C52",
        "purpledev" : "#8F72B5",
        "whiteabtme" : "#E1EBED",
        "purplecard" : "#DCCFED"
        
      }
    },
 
  },
  plugins: [],
};
export default config;
