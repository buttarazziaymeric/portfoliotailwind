export interface Project {
    _id: string;
    _createdAt: string
    title: string;
    description: string;
    photo: {
        asset:{
            url: string;
        };
    };
    visibility: boolean;
    link: string;
    slug:{
        current: string
    };
}