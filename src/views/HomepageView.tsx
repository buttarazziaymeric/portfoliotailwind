import React from 'react'
import Layout from '../components/Layout/Layout'
import { Project } from '@/typing';
import Aboutme from '../components/Homepage/Aboutme';
import Banner from '../components/Homepage/Banner';
import Projects from '../components/Homepage/Projects';
import ContactForm from '../components/Homepage/ContactForm';
import Footer from '../components/Layout/Footer';


interface Props {
  projects: Project[];
}

export default function Home({projects}:Props) {

  return (
    <Layout>
      <Banner/>
      <Aboutme/>
      <Projects projects={projects}/>
      <ContactForm/>
      <Footer/>

      
    </Layout>
  );
}

