import React from 'react'
import Layout from '../components/Layout/Layout'
import { Project } from '@/typing';
import ProjectBanner from '../components/Pageproject/ProjectBanner';

interface Props{
  project: Project;
}

function PageprojectView({project}:Props) {
  return (
    <Layout>
      <ProjectBanner project={project}/>
    </Layout>
  )
}

export default PageprojectView
