import React from "react";
import TechnoListItem from "./TechnoListItem";

function Aboutme() {
return (
    <section className={`flex w-full py-10 bg-whiteabtme`}>
        <div className={`w-5/6 mx-auto `}>

    {/* Titre à propos */}
        <div className={`flex flex-col w-1/3 gap-y-3 `}>
            <h2 className={`text-[#331C52] text-4xl `}>{`À propos`}</h2>
            <p className={`text-[#331C52] text-2xl font-medium`}>{`Je m'appelle Aymeric Buttarazzi, 26 ans, en pleine formation chez Simplon pour devenir développeur web et mobile. Passionné par la technologie, je m'efforce de maîtriser les compétences nécessaires pour créer des applications web et mobiles efficaces et intuitives, visant à enrichir l'expérience utilisateur.`}</p>
        </div>

    {/* Compétences titre */}
        <div className={`flex flex-col w-1/3 gap-y-3 ml-auto `}>
            <h2 className={`text-[#331C52] text-4xl`}>{`Compétences`}</h2>
                <p className={` text-[#331C52] text-2xl font-medium`}>{`Techonologies`}</p>



    
    {/* Logo  techno*/ }
        <ul className={`grid grid-cols-5 gap-2`}>
        <TechnoListItem/>
        <TechnoListItem/>
        <TechnoListItem/>
        <TechnoListItem/>
        <TechnoListItem/>
        <TechnoListItem/>
        <TechnoListItem/>
        <TechnoListItem/>
        <TechnoListItem/>
        <TechnoListItem/>
        </ul>
        </div>
        </div>
    </section>
);
}

export default Aboutme;
