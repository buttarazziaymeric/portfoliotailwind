import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useState } from "react";

function Formpage() {
  const [submitted, setSubmitted] = useState(false);
  const [success, setSuccess] = useState(false);

  const validation = yup.object().shape({
    firstName: yup.string().required("Veuillez saisir votre prénom"),
    lastName: yup.string().required("Veuillez saisir votre nom"),
    email: yup.string().email().required("Veuillez saisir votre email"),

    message: yup.string().required("Veuillez saisir votre message"),
  });
  const optionsForm = { resolver: yupResolver(validation) };
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm(optionsForm);

  const submitForm = async (data: any) => {
    const { firstname, lastname, email, message } = data;
    console.log(data);
    setSubmitted(true);

    await fetch("/api/createContactMessage", {
      method: "POST",
      body: JSON.stringify({
        firstname,
        lastname,
        email,
        message,
      }),
    })
      .then((res) => {
        
        if (res.status === 200) {
          setSuccess(true);
    
        } else {
          setSuccess(false);
          console.log("Erreur !!!");
        }
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        setSubmitted(false);
      });
  };

  return (
    <div className="font-all bg-orange-100 h-screen flex items-center justify-center">
      <div className="bg-white rounded-lg w-3/4 lg:w-1/3 p-20 flex flex-col items-center">
        <h1 className="text-4xl mb-8 text-vert">Contactez moi</h1>
        {success ?
        <div>success</div>
        :
          <form onSubmit={handleSubmit(submitForm)} className=
        {`w-full space-y-4
        ${submitted && "pointer-events-none animate-pulse"}`}>
          <div className="flex flex-col">
            <label className="mb-2 text-lg text-vert ml-4">Nom:</label>
            <input {...register("lastName")} type="text" className="p-2 bg-slate-200 border border-orange-600 rounded-full" />
            {errors.lastName && <p>{errors.lastName.message}</p>}
          </div>
          <div className="flex flex-col">
            <label className="mb-2 text-lg text-vert ml-4">Prénom:</label>
            <input {...register("firstName")} type="text" className="p-2 bg-slate-200 border border-orange-600 rounded-full" />
          </div>
          <div className="flex flex-col">
            <label className="mb-2 text-lg text-vert ml-4">E-mail:</label>
            <input {...register("email")} type="email" className="p-2 bg-slate-200 border border-orange-600 rounded-full" />
          </div>
          <div className="flex flex-col">
            <label className="mb-2 text-lg text-vert ml-4">Message:</label>
            <textarea {...register("message")} className="p-2 h-32 bg-slate-200 border border-orange-600 rounded-md" />
          </div>
          <div className="flex justify-center">
            <input type="submit" value="Envoyer" className="mt-4 bg-orange-600 text-white px-12 py-3 rounded-full cursor-pointer text-lg" />
          </div>
        </form>}
      </div>
    </div>
  );
}

export default Formpage;