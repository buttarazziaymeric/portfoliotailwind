import React from 'react'

function TechnoListItem() {
  return (
    <li className='w-full aspect-square rounded-xl bg-purpleouais'>
        <i className="fa fa-coffee"></i>    
    </li>
  )
}

export default TechnoListItem
