import React, { useState } from "react";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useForm } from "react-hook-form";




function ContactForm() {
  const [submitted, setSubmitted] = useState(false);
  const [success, setSuccess] = useState(false);

  const validation = yup.object().shape({
    firstname: yup.string().required("Veuillez saisir votre prénom"),
    lastname: yup.string().required("Veuillez saisir votre nom"),
    email: yup.string().email().required("Veuillez saisir votre email"),
    message: yup.string().required("Veuillez saisir votre message"),
  });

  const optionsForm = { resolver: yupResolver(validation) };
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm(optionsForm);

  const submitForm = async (data: any) => {
    const { firstname, lastname, email, message } = data;
    console.log(data);
    setSubmitted(true);

    await fetch("/api/createMessageForm", {
      method: "POST",
      body: JSON.stringify({
        firstname,
        lastname,
        email,
        message,
      }),
    })
      .then((res) => {
        
        if (res.status === 200) {
          setSuccess(true);
    
        } else {
          setSuccess(false);
          console.log("Erreur !!!");
        }
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        setSubmitted(false);
      });
  };
  
  return (
    <main className="w-full h-screen bg-purplehello flex flex-col items-center justify-center px-8">
      <div className="w-full max-w-lg bg-purplee p-8 rounded-lg shadow-lg">
        <h2 className="text-3xl font-bold text-white mb-6 text-center">Contactez-moi</h2>
        {success ?
        <div>success</div>
        :
        <form onSubmit={handleSubmit(submitForm)} className=
        {`space-y-6
        ${submitted && "pointer-events-none-pulse"}`}>
         <div className="flex flex-col">
            <label className="mb-2 text-lg text-vert ml-4">Nom:</label>
            <input {...register("lastname")} type="text" className="p-2 bg-gray-400 border border-purpleouais rounded-full" />
            {errors.lastname && <p>{errors.lastname.message}</p>}
          </div>
          <div className="flex flex-col">
            <label className="mb-2 text-lg text-vert ml-4">Prénom:</label>
            <input {...register("firstname")} type="text" className="p-2 bg-gray-400 border border-purpleouais rounded-full" />
          </div>
          <div className="flex flex-col">
            <label className="mb-2 text-lg text-vert ml-4">E-mail:</label>
            <input {...register("email")} type="email" className="p-2 bg-gray-400 border border-purpleouais rounded-full" />
          </div>
          <div className="flex flex-col">
            <label className="mb-2 text-lg text-vert ml-4">Message:</label>
            <textarea {...register("message")} className="p-2 h-32 bg-gray-400 border border-purpleouais rounded-md" />
          </div>
          <div className="flex justify-center">
            <input type="submit" value="Envoyer" className="mt-4 bg-purple-600 hover:bg-purple-500 text-white px-12 py-3 rounded-full cursor-pointer text-lg" />
          </div>
        </form>}
      </div>
    </main>
  );
}

export default ContactForm;
