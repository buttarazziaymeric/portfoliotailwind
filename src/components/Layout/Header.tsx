import { EnvelopeIcon } from '@heroicons/react/16/solid';
import React from 'react';

function Header() {
  return (
    <header className='flex w-full p-10 justify-between bg-gradient-to-r from-purpleouais to-purplee'>
      <div className='w-4/5 flex items-center justify-between'>
        <div className='text-white font-bold text-xl'>
          Logo
        </div>
        <div className='flex gap-11 text-white'>
          <span>Home</span>
          <span>About</span>
          <span>Skills</span>
          <span>Projects</span>
        </div>
        <div className='flex items-center'>
          <button className='bg-white text-purpleouais text-sm font-semibold px-4 py-2 rounded-lg mr-4'>
            Hire me
          </button>
          <EnvelopeIcon className='h-6 w-6 text-white' />
        </div>
      </div>
    </header>
  );
}

export default Header;
