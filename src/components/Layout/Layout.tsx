import React from 'react'
import Header from './Header'
import Head from 'next/head'
import { useRouter } from 'next/router'
import Banner from '../Homepage/Banner'
import Aboutme from '../Homepage/Aboutme'
import Projects from '../Homepage/Projects'
import Footer from './Footer'

function Layout({children,
    title = "Portfolio",
    description = "Lorem Ipsum test description",
    keywords = "Projet test, description"}: any)
{
    const router= useRouter ()

  return (
    <div
    className='flex flex-col bg-white text-white flex-1 h-screen scroll-smooth w-full'>
<Head>
<meta name='viewport' content='width=device-width, initial-scale=1'/>
<meta name='keywords' content={keywords}/>
<meta name='description' content={description}/>
<meta charSet='utf-8'/>
<title>{title}</title>
<link rel='icon' href=""/>
</Head>

<Header/>
<main
className='flex flex-col flex-1 w-full h-screen'>
{children}
</main>



    </div>
  )
}

export default Layout