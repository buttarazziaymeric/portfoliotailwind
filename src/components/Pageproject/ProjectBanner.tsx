import { urlFor } from '@/sanity';
import Image from 'next/image';
import React from 'react';

function ProjectBanner({ project }: any) {
  return (
    <main className="fixed w-full h-4/5 bg-purplehello text-white flex flex-col justify-start items-center p-4 overflow-auto">
      <div className="text-center mt-8">
        <h1 className="font-bold text-4xl md:text-5xl mb-4">{project?.title}</h1>
        <p className="text-lg md:text-xl mb-6">{project?.description}</p>
      </div>
      <div className="relative w-1/2 h-auto mt-6">
        <Image
          src={urlFor(project?.photo).url()!}
          layout="responsive"
          width={500}
          height={500}
          className="object-cover rounded-md shadow-lg"
          alt={project?.title || 'Project Image'}
        />
      </div>
    </main>
  );
}

export default ProjectBanner;
