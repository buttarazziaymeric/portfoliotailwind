import Image from "next/image";
import { Inter } from "next/font/google";
import HomepageView from "@/src/views/HomepageView";
import { Project } from "@/typing";
import { sanityClient } from "../sanity";



interface Props{
  projects: Project[];
}
export default function Home({projects}:Props) {
  console.log(projects);
  return (
    <>
    <HomepageView
    projects={projects}/>
    </>
  );
}

export const getServerSideProps = async () => {
  const projectQuery = `*[_type == "project"]{
_id,
title,
description,
photo,
visibility,
_createdAt,
link,
slug{
current
}}`;

const projects = await sanityClient.fetch(projectQuery)

return {
  props: {
    projects,
  }
}

};
