import { sanityClient } from "@/sanity";
import PageprojectView from "@/src/views/PageprojectView";
import { Project } from "@/typing";
import { GetStaticProps } from "next";
import React from "react";

interface Props {
  project: Project;
}

function OneProject({ project }: Props) {
  return (
    <PageprojectView
    project={project}/>
  );
}

export default OneProject;

export async function getStaticPaths() {
  const query = `*[_type == "project"]{
        _id,
        slug {
            current
        }
    } `;

  const projects = await sanityClient.fetch(query);

  const paths = projects
    .map((project: any) => ({
      params: {
        slug: project?.slug?.current,
      },
    }))

    .flat();

  return {
    paths,
    fallback: "blocking",
  };
}

export const getStaticProps: GetStaticProps = async ({ params }: any) => {
  const projectQuery = `*[_type == "project" &&  slug.current == $slug ][0] {
    _id,
    title,
    description,
    photo,
    visibility,
    _createdAt,
    link,
    slug{
    current
    }
    }`;

  const project = await sanityClient.fetch(projectQuery, {
    slug: params?.slug,
  });

  if (!project) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      project,
    },
    revalidate: 10,
  };
};